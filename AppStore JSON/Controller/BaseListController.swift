//
//  BaseListController.swift
//  AppStore JSON
//
//  Created by Уали on 6/24/19.
//  Copyright © 2019 Ualikhan. All rights reserved.
//

import UIKit
class BaseListController: UICollectionViewController {
    // need code ???
    init() {
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
