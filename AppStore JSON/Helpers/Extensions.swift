//
//  Extensions.swift
//  AppStore JSON
//
//  Created by Уали on 7/1/19.
//  Copyright © 2019 Ualikhan. All rights reserved.
//

import UIKit

// need code ???
extension UILabel {
    convenience init(text: String, font: UIFont, numberOfLines: Int = 1) {
        self.init(frame: .zero)
        self.text = text
        self.font = font
        self.numberOfLines = numberOfLines
    }
}

extension UIImageView {
    convenience init(cornerRadius: CGFloat){
        self.init(image: nil)
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFill
    }
}

extension UIButton {
    convenience init(title: String){
        self.init(type: .system)
        self.setTitle(title,for: .normal)
    }
}
