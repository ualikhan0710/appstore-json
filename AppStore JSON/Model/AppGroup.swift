//
//  AppGroup.swift
//  AppStore JSON
//
//  Created by Уали on 7/3/19.
//  Copyright © 2019 Ualikhan. All rights reserved.
//

import Foundation

struct AppGroup: Decodable { 
    let feed: Feed
}

struct Feed: Decodable {
    let title: String
    let results: [FeedResult]
}

struct FeedResult: Decodable {
    let  id, name, artistName, artworkUrl100: String
}
