//
//  SocialApp.swift
//  AppStore JSON
//
//  Created by Уали on 7/3/19.
//  Copyright © 2019 Ualikhan. All rights reserved.
//

import Foundation

struct SocialApp: Decodable {
    let id, name, imageUrl, tagline: String
}
