//
//  TodayItem.swift
//  AppStore JSON
//
//  Created by Уали on 7/8/19.
//  Copyright © 2019 Ualikhan. All rights reserved.
//

import UIKit

struct TodayItem {
    let category: String
    let title: String
    let image: UIImage
    let description: String
    let backgroundColor: UIColor
    
    // enum
    let cellType : CellType
    
    let apps: [FeedResult]
    
    enum CellType: String {
        case single, multiple
    }
}
