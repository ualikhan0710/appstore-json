//
//  Service.swift
//  AppStore JSON
//
//  Created by Уали on 6/20/19.
//  Copyright © 2019 Ualikhan. All rights reserved.
//

import Foundation

class Service {
    static let shared = Service() // singleton
    
    func fetchApps(searchTerm: String, completion: @escaping (SearchResult?, Error?)-> () ){
        print("Fetching iTunes apps from service layer")
        let urlString = "https://itunes.apple.com/search?term=\(searchTerm)&entity=software"
        
        fetchGenericJSONData(urlString: urlString, completion: completion)
        
    }
    
    func fetchTopGrossing(completion: @escaping (AppGroup? , Error? ) -> ()){
        let urlString = "https://rss.itunes.apple.com/api/v1/us/ios-apps/top-grossing/all/50/explicit.json"
        
        fetchAppGroup(urlString: urlString, completion: completion)
    }
    
    func fetchGames(completion: @escaping (AppGroup? , Error? ) -> ()){
        let urlString = "https://rss.itunes.apple.com/api/v1/us/ios-apps/new-games-we-love/all/50/explicit.json"
        
        fetchAppGroup(urlString: urlString, completion: completion)
    }
    
    //helper
    func fetchAppGroup(urlString: String,completion: @escaping (AppGroup? , Error? ) -> Void) {
       fetchGenericJSONData(urlString: urlString, completion: completion)
    }
    
    func fetchSocialApps(completion: @escaping ([SocialApp]?, Error?) -> Void) {
        let urlString = "https://api.letsbuildthatapp.com/appstore/social"
        
        fetchGenericJSONData(urlString: urlString, completion: completion)
    }
    
    
    // declare my generic json function here
    func fetchGenericJSONData<T: Decodable>(urlString: String , completion: @escaping (T?, Error?) -> () ){
        
        guard let url = URL(string: urlString) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            
            if let err = err {
                completion(nil, err)
                return
            }
            do {
                let objects = try JSONDecoder().decode(T.self, from: data!)
                completion(objects,nil)
            } catch {
                completion(nil, error)
                print("Failed to decode:",error)
            }
            }.resume() // this will fire your request
    }
    
}

/*
 
 func fetchApps(searchTerm: String, completion: @escaping ([Result], Error?)-> () ){
 print("Fetching iTunes apps from service layer")
 
 let urlString = "https://itunes.apple.com/search?term=\(searchTerm)&entity=software"
 
 
 guard let url = URL(string: urlString) else {return}
 
 //fetch data from internet
 URLSession.shared.dataTask(with: url) { (data, resp, err) in
 
 if let err = err{
 print("Failed to fetch apps:", err)
 completion([], nil)
 return
 }
 
 //success
 guard let data = data else { return }
 
 do {
 let searchResult = try JSONDecoder().decode(SearchResult.self, from: data)
 
 completion(searchResult.results, nil)
 //                self.appResults = searchResult.results
 //
 //                DispatchQueue.main.async {
 //                    self.collectionView.reloadData()
 //                }
 
 }catch let jsonError{
 print("Failed to decode json", jsonError)
 completion([], jsonError )
 }
 }.resume() //fires of the request
 
 }
 
 
 
 
// example for <T>
// Stack
// Generic is to declate the Type later on

//class Stack<T> {
class Stack<T: Decodable> {
    var items = [T]()
    
    func push(item: T) {
        items.append(item)
    }
    
    func pop() -> T?  {
        return items.last
    }
}

import UIKit

func dummyFunc() {
    
    
    let stackOfStrings = Stack<String>()
    stackOfStrings.push(item: "has to be String")
    
    let stackOfInts = Stack<Int>()
    stackOfInts.push(item: 1)
}
 
*/
